# Behat web testing training

Used technologies:
Behat 2.4 + Mink 1.4

for more info see composer.json file

## Usage 

Clone this repo:

``` bash
git clone git@bitbucket.org:MateuszStafiej/php_behat.git
```

Now install Behat, Mink, MinkExtension and their dependencies with [composer](http://getcomposer.org/):

``` bash
curl http://getcomposer.org/installer | php
php composer.phar install
```

Download latest Selenium2 jar from the [Selenium website](http://seleniumhq.org/download/)

Run selenium jar with:

    ``` bash
    java -jar selenium.jar > /dev/null &
    ```

Now if you run:

``` bash
vendor/bin/behat
```

you should see an output like this:

``` gherkin
Feature: Search
  In order to see a word definition
  As a website user
  I need to be able to search for a word

  Scenario: Searching for a page that does exist
    Given I am on /wiki/Main_Page
    When I fill in "search" with "Behavior Driven Development"
    And I press "searchButton"
    Then I should see "agile software development"

  Scenario: Searching for a page that does NOT exist
    Given I am on /wiki/Main_Page
    When I fill in "search" with "Glory Driven Development"
    And I press "searchButton"
    Then I should see "Search results"

  @javascript
  Scenario: Searching for a page with autocompletion
    Given I am on /wiki/Main_Page
    When I fill in "search" with "Behavior Driv"
    And I wait for the suggestion box to appear
    Then I should see "Behavior Driven Development"

3 scenarios (3 passed)
12 steps (12 passed)
```
