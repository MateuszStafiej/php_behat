Feature: Wikipedia Search
  In order to see a word definition
  As a website user
  I need to be able to search for a word

  @javascript
  Scenario: Searching for a page that does exist
    Given I am on "http://wikipedia.com/wiki/Main_Page"
    When I fill in "search" with "Behavior Driven Development"
    And I press "searchButton"
    Then I should see "software development process" appear

  @javascript
  Scenario: Searching for a page that does NOT exist
    Given I am on "http://wikipedia.com/wiki/Main_Page"
    When I fill in "search" with "Glory Driven Development"
    And I press "searchButton"
    Then I should see "Search results" appear

  @javascript
  Scenario: Searching for a page with autocompletion
    Given I am on "http://wikipedia.com/wiki/Main_Page"
    When I fill in "search" with "Behavior Driv"
    And I wait for the suggestion box to appear
    Then I should see "Behavior-Driven Development" appear

@javascript
      Scenario: performance
        Given I am on "https://google.pl"
        When I fill in "lst-ib" with "urufukuro blog"
        And I press "Szukaj"
        Then I should see "Urufukuro" appear



#dupa dpsafnhklahfjkdshlkvdzlkgkjfd